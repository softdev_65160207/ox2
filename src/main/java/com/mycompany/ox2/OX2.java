/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.ox2;

import java.util.Scanner;

/**
 *
 * @author Snow
 */
public class OX2 {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char player = 'X';
    static int row, col;

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printTable();
            printTurn();
            inputColum();

            if (iswin()) {
                printTable();
                printWin();
                break;
            }
            swtichplayer();
        }

    }

    private static void printWelcome() {
        System.out.println("Welcome to xo!");
    }

    private static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private static void printTurn() {
        System.out.println(player + " Turn");
    }

    private static void inputColum() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please input row, col: ");
            row = sc.nextInt();
            col = sc.nextInt();
            if (table[row - 1][col - 1] == '-') {
                table[row - 1][col - 1] = player;
                return;
            }
        }

    }

    private static void swtichplayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    private static boolean iswin() {
        if (CheckRow('O')) {
            return true;
        } else if (CheckCol('X')) {
            return true;
        }
        return false;
    }

    private static void printWin() {
        System.out.print(player + " WIN!");
    }

    private static boolean CheckRow(char symbol) {
        if ((table[0][0] == symbol && table[0][1] == symbol && table[0][2] == symbol)
                || (table[1][0] == symbol && table[1][1] == symbol && table[1][2] == symbol)
                || (table[2][0] == symbol && table[2][1] == symbol && table[2][2] == symbol)
                || (table[0][0] == symbol && table[1][0] == symbol && table[2][0] == symbol)
                || (table[0][1] == symbol && table[1][1] == symbol && table[2][1] == symbol)
                || (table[0][2] == symbol && table[1][2] == symbol && table[2][2] == symbol)
                || (table[0][0] == symbol && table[1][1] == symbol && table[2][2] == symbol)
                || (table[0][2] == symbol && table[1][1] == symbol && table[2][0] == symbol)) {
            return true;
        }
        return false;
    }

    private static boolean CheckCol(char symbol) {
        if ((table[0][0] == symbol && table[0][1] == symbol && table[0][2] == symbol)
                || (table[1][0] == symbol && table[1][1] == symbol && table[1][2] == symbol)
                || (table[2][0] == symbol && table[2][1] == symbol && table[2][2] == symbol)
                || (table[0][0] == symbol && table[1][0] == symbol && table[2][0] == symbol)
                || (table[0][1] == symbol && table[1][1] == symbol && table[2][1] == symbol)
                || (table[0][2] == symbol && table[1][2] == symbol && table[2][2] == symbol)
                || (table[0][0] == symbol && table[1][1] == symbol && table[2][2] == symbol)
                || (table[0][2] == symbol && table[1][1] == symbol && table[2][0] == symbol)) {
            return true;
        }
        return false;

    }
}
